extends Control

func _on_Panel_pressed():
	$Panel.show()
	get_tree().paused = true

func _on_Continue_pressed():
	$Panel.hide()
	get_tree().paused = false
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

