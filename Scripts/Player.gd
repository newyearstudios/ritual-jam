extends KinematicBody

const GRAVITY = -50
var vel = Vector3()
const MAX_SPEED = 13
const JUMP_SPEED = 18
const ACCEL= 5.5

var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var floor_normal = Vector2( 0, 0 )

var slope_stop_min_velocity = 5

var camera
var rotation_helper

var MOUSE_SENSITIVITY = 0.15

const MAX_SPRINT_SPEED = 17
const SPRINT_ACCEL = 20
var is_sprinting = false

var flashlight

var globals

var grounded = false

func _ready():
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper
	
	flashlight = $Rotation_Helper/Flashlight

func _physics_process(delta):
	process_input(delta)
	process_movement(delta)

func process_input(delta):

	# ----------------------------------
	# Walking
	dir = Vector3()
	var cam_xform = camera.get_global_transform()

	var input_movement_vector = Vector2()
	
	var is_moving = false
	
	var anim_player = get_node("Arms/Rig/Walk")

	if Input.is_action_pressed("movement_forward"):
		input_movement_vector.y += 1
		is_moving = true
	if Input.is_action_pressed("movement_backward"):
		input_movement_vector.y -= 1
		is_moving = true
	if Input.is_action_pressed("movement_left"):
		input_movement_vector.x -= 1
		is_moving = true
	if Input.is_action_pressed("movement_right"):
		input_movement_vector.x = 1
		is_moving = true

	# ----------------------------------
	# Animations
	# ----------------------------------
#	if Input.is_action_just_pressed("movement_forward") || Input.is_action_just_pressed("movement_backward") || Input.is_action_just_pressed("movement_left") || Input.is_action_just_pressed("movement_right"):
#		get_node("Arms/Rig/Walk").play("Walk")
#
#	else:
#		get_node("Arms/Idle").play("Idle")

	var anim_to_play = "Idle"
	
	if is_moving:
		anim_to_play = "Walk"
		
	var current_anim = anim_player.get_current_animation()
	if current_anim != anim_to_play:
		anim_player.play("Walk")

	input_movement_vector = input_movement_vector.normalized()

	dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	dir += cam_xform.basis.x.normalized() * input_movement_vector.x

	
	# ----------------------------------
	# Turning the flashlight on/off
	# ----------------------------------
	if Input.is_action_just_pressed("flashlight"):
		if flashlight.is_visible_in_tree():
			flashlight.hide()
		else:
			flashlight.show()

	# ----------------------------------
	# Jumping
	# ----------------------------------
	if is_on_floor():
		if Input.is_action_just_pressed("movement_jump"):
			vel.y = JUMP_SPEED

	# ----------------------------------
	# Sprinting
	# ----------------------------------
	if Input.is_action_pressed("movement_sprint"):
		is_sprinting = true
	else:
		is_sprinting = false
# ----------------------------------

# ----------------------------------

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()

	vel.y += delta*GRAVITY

	var hvel = vel
	hvel.y = 0

	var target = dir
	#target *= MAX_SPEED
	if is_sprinting:
		target *= MAX_SPRINT_SPEED
	else:
		target *= MAX_SPEED

	var accel
	if dir.dot(hvel) > 0:
		#accel = ACCEL
		if is_sprinting:
			accel = SPRINT_ACCEL
		else:
			accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel,Vector3(0,1,0), 2.05, 4, deg2rad(MAX_SLOPE_ANGLE))

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))

		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -70, 70)
		rotation_helper.rotation_degrees = camera_rot