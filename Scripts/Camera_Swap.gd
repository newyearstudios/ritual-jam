extends Area
 
func _ready():
	connect("body_entered", self, "enable_camera")


func _on_Cam_Swap_Area_body_entered(body):
#    if body.name != "Player":
#        return
	if has_node("Camera"):
		var cam = get_node("Camera")
		cam.make_current()
		if cam.has_method("set_target"):
			cam.set_target(body)