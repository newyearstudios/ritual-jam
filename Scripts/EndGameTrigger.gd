extends Area

func _on_Area_body_entered(body):
	get_tree().change_scene('res://Menus/EndGame.tscn')

	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)