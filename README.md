# Ritual-Jam

Segmentation Fault is a simple walking simulator with a subtle eerie story unfolding as the player navigates the depths of a cave in an attempt to record and gain understanding of what happened to their expedition team.

This is newyear Studios first project. We have enjoyed using Godot 3.0 and other open source software to produce this title. 

As we move forward you can expect similar yet more intricate projects from us with a focus on FOSS tools and providing source via GitLab. 

We hope this project can be used as a learning tool for others as much as creating it was for us. Enjoy.